import ast
import boto3
import logging
import os
import sys
import traceback
import time

LOG_FILE_NAME = 'output.log'

REGION = 'us-west-2'

class S3Handler:
    """S3 handler."""

    def __init__(self):
        self.client = boto3.client('s3')

        logging.basicConfig(filename=LOG_FILE_NAME,
                            level=logging.DEBUG, filemode='w',
                            format='%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        self.logger = logging.getLogger("S3Handler")

    def help(self):
        print("Supported Commands:")
        print("1. createdir <bucket_name>")
        print("2. upload <source_file_name> <bucket_name> [<dest_object_name>]")
        print("3. download <dest_object_name> <bucket_name> [<source_file_name>]")
        print("4. delete <dest_object_name> <bucket_name>")
        print("5. deletedir <bucket_name>")
        print("6. find <file_extension> [<bucket_name>] -- e.g.: 1. find txt  2. find txt bucket1 --")
        print("7. listdir [<bucket_name>]")
    
    def _error_messages(self, issue):
        error_message_dict = {}
        error_message_dict['incorrect_parameter_number'] = 'Incorrect number of parameters provided'
        error_message_dict['not_implemented'] = 'Functionality not implemented yet!'
        error_message_dict['bucket_name_exists'] = 'Directory already exists.'
        error_message_dict['bucket_name_empty'] = 'Directory name cannot be empty.'
        error_message_dict['bucket_not_empty'] = 'Directory must be empty before deletion.'
        error_message_dict['missing_source_file'] = 'Source file cannot be found.'
        error_message_dict['non_existent_bucket'] = 'Directory does not exist.'
        error_message_dict['non_existent_object'] = 'Destination Object does not exist.'
        error_message_dict['unknown_error'] = 'Something was not correct with the request. Try again.'

        if issue:
            return error_message_dict[issue]
        else:
            return error_message['unknown_error']

    def _get_file_extension(self, file_name):
        if os.path.exists(file_name):
            return os.path.splitext(file_name)[-1]
        else:
            return ''

    def _get(self, bucket_name):
        response = ''
        try:
            response = self.client.head_bucket(Bucket=bucket_name)
        except Exception as e:
            # print(e)
            # traceback.print_exc(file=sys.stdout)

            response_code = e.response['Error']['Code']
            if response_code == '404' or response_code == '403':
                return False
            elif response_code == '200':
                return True
            else:
                raise e
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            return False

    def _get_object(self, object_name, bucket_name):
        response = ''
        try:
            response = self.client.get_object(Bucket=bucket_name, Key=object_name)
        except Exception as e:
            response_code = e.response['Error']['Code']
            if response_code == 'NoSuchKey':
                return False
            if response_code == 'NoSuchBucket':
                return False
            else:
                raise e
        return True

    def _move_file(self, file_name):
        time_stamp = int(round(time.time() * 1000))
        backup_file_name = '{}.bak.{}'.format(file_name, time_stamp)
        os.rename(file_name, backup_file_name)

    def _dir_empty(self, bucket_name):
        response = ''
        try:
            response = self.client.list_objects_v2(Bucket=bucket_name)
            if response['KeyCount']:
                return False
            else:
                return True
        except Exception as e:
            raise e

    def _extention_matches(self, file_extension, object_name, bucket_name):
        response = ''
        try:
            response = self.client.get_object(Bucket=bucket_name, Key=object_name)
            if file_extension == response['Metadata']['file-ext'][1:]:
                return True
            else:
                return False
        except Exception as e:
            raise e

    def _list_files(self, bucket_name, continuation_token='', prev_objects=[]):
        object_names = []
        response = ''
        try:
            if continuation_token:
                response = self.client.list_objects_v2(Bucket=bucket_name, ContinuationToken=continuation_token)
            else:
                response = self.client.list_objects_v2(Bucket=bucket_name)
            if 'Contents' in response:
                object_names = list(obj['Key'] for obj in response['Contents'])
            if 'NextContinuationToken' in response:
                next_token = response['NextContinuationToken']
                object_names = self._list_files(bucket_name, next_token, object_names)
            return prev_objects + object_names
        except Exception as e:
            raise e

    def _list_directories(self):
        bucket_names = []
        try:
            response = self.client.list_buckets()
            if 'Buckets' in response:
                bucket_names = list(bucket['Name'] for bucket in response['Buckets'])
            return bucket_names
        except Exception as e:
            raise e

    def _list_files_with_ext(self, file_extension, bucket_name, object_names):
        try:
            object_keys = self._list_files(bucket_name)
            # Search for files with extension <file_extension>
            for key in object_keys:
                if self._extention_matches(file_extension, key, bucket_name):
                    object_names.append(key)
        except Exception as e:
            raise e


    def createdir(self, bucket_name):
        if not bucket_name:
            return self._error_messages('bucket_name_empty')

        try:
            if self._get(bucket_name):
                return self._error_messages('bucket_name_exists')
            self.client.create_bucket(Bucket=bucket_name,
                                      CreateBucketConfiguration={'LocationConstraint': REGION})
        except Exception as e:
            response_code = e.response['Error']['Code']
            if response_code == '403':
                return self._error_messages('bucket_name_exists')
            else:
                print(e)
                return self._error_messages('unknown_error')

        # Success response
        operation_successful = ('Directory %s created.' % bucket_name)
        return operation_successful

    def listdir(self, bucket_name=''):
        contents = []

        # If bucket_name is provided, check that bucket exits.
        if bucket_name:
            # Validation
            try:
                if not self._get(bucket_name):
                    return self._error_messages('non_existent_bucket')
            except Exception as e:
                print(e)
                return self._error_messages('unknown_error')
            # Query files in directory
            try:
                contents = self._list_files(bucket_name)
            except Exception as e:
                print(e)
                return self._error_messages('unknown_error')
        # If bucket_name is empty then display the names of all the buckets
        else:
            try:
                contents = self._list_directories()
            except Exception as e:
                print(e)
                return self._error_messages('unknown_error')
        
        # If bucket_name is provided then display the names of all objects in the bucket
        operation_successful = ''
        for i, content in enumerate(contents):
            operation_successful += content
            if i < len(contents) - 1:
                operation_successful += ', '
        return operation_successful

    def upload(self, source_file_name, bucket_name, dest_object_name=''):
        # 1. Parameter Validation
        #    - source_file_name exits in current directory
        #    - bucket_name exists
        if not os.path.exists(source_file_name):
            return self._error_messages('missing_source_file')
        try:
            if not self._get(bucket_name):
                return self._error_messages('non_existent_bucket')
        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')

        # 2. If dest_object_name is not specified then use the source_file_name as dest_object_name
        obj_key = ''
        if dest_object_name:
            obj_key = dest_object_name
        else:
            obj_key = os.path.basename(os.path.normpath(source_file_name))
        
        # 3. SDK call
        #    - When uploading the source_file_name and add it to object's meta-data
        #    - Use self._get_file_extension() method to get the extension of the file.
        file_ext = self._get_file_extension(source_file_name)
        meta_data = {'Metadata': {'file-ext': file_ext}}
        
        try:
            self.client.upload_file(
                source_file_name, bucket_name, obj_key,
                ExtraArgs = meta_data)

        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')

        # Success response
        operation_successful = ('File %s uploaded to bucket %s.' % (source_file_name, bucket_name))
        return operation_successful


    def download(self, dest_object_name, bucket_name, source_file_name=''):
        # if source_file_name is not specified then use the dest_object_name as the source_file_name
        # If the current directory already contains a file with source_file_name then move it as a backup
        # with following format: <source_file_name.bak.current_time_stamp_in_millis>

        # Parameter Validation
        try:
            if not self._get(bucket_name):
                return self._error_messages('non_existent_bucket')
            if not self._get_object(dest_object_name, bucket_name):
                return self._error_messages('non_existent_object')
        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')
        
        if not source_file_name:
            source_file_name = dest_object_name
        if os.path.exists(source_file_name):
            self._move_file(source_file_name)

        # SDK Call
        try:
            self.client.download_file(bucket_name, dest_object_name, source_file_name)
        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')

        # Success response
        operation_successful = ('Object %s downloaded from bucket %s.' % (dest_object_name, bucket_name))
        return operation_successful


    def delete(self, dest_object_name, bucket_name):
        try:
            if not self._get(bucket_name):
                return self._error_messages('non_existent_bucket')
            if not self._get_object(dest_object_name, bucket_name):
                return self._error_messages('non_existent_object')
        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')

        try:
            self.client.delete_object(Bucket=bucket_name, Key=dest_object_name)
        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')

        # Success response
        operation_successful = ('Object %s deleted from bucket %s.' % (dest_object_name, bucket_name))
        return operation_successful


    def deletedir(self, bucket_name):
        # Delete the bucket only if it is empty
        try:
            if not self._get(bucket_name):
                return self._error_messages('non_existent_bucket')
            if not self._dir_empty(bucket_name):
                return self._error_messages('bucket_not_empty')
        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')

        try:
            self.client.delete_bucket(Bucket=bucket_name)
        except Exception as e:
            print(e)
            return self._error_messages('unknown_error')
        
        # Success response
        operation_successful = ("Deleted bucket %s." % bucket_name)
        return operation_successful


    def find(self, file_extension, bucket_name=''):
        # Return object names that match the given file extension
        object_names = []

        # If bucket_name is specified then search for objects in that bucket.
        # If bucket_name is empty then search all buckets
        if bucket_name:
            # Validate bucket exists
            try:
                if not self._get(bucket_name):
                    return self._error_messages('non_existent_bucket')
            except Exception as e:
                print(e)
                return self._error_messages('unknown_error')
            # Query files from directory
            try:
                self._list_files_with_ext(file_extension, bucket_name, object_names)
            except Exception as e:
                print(e)
                return self._error_messages('unknown_error')
        else:
            # Query files from all directories
            try:
                buckets = self._list_directories()
                for bucket in buckets:
                    self._list_files_with_ext(file_extension, bucket, object_names)
            except Exception as e:
                print(e)
                return self._error_messages('unknown_error')

        operation_successful = ''
        if object_names:
            for i, object_name in enumerate(object_names):
                operation_successful += object_name
                if i < len(object_names) - 1:
                    operation_successful += ', '
        else:
            operation_successful = 'No Files Found'
        return operation_successful


    def dispatch(self, command_string):
        parts = command_string.split(" ")
        response = ''

        if parts[0] == 'createdir':
            argc = len(parts)
            if argc < 2:
                # Parameter Validation
                # - Bucket name is not empty
                response = self._error_messages('bucket_name_empty')
            elif argc > 2:
                response = self._error_messages('incorrect_parameter_number')
            else:
                # Figure out bucket_name from command_string
                bucket_name = parts[1]
                response = self.createdir(bucket_name)

        elif parts[0] == 'upload':
            # Figure out parameters from command_string
            # source_file_name and bucket_name are compulsory; dest_object_name is optional
            # Use self._error_messages['incorrect_parameter_number'] if number of parameters is less
            # than number of compulsory parameters
            source_file_name = ''
            bucket_name = ''
            dest_object_name = ''

            argc = len(parts)
            if argc < 3 or argc > 4:
                response = self._error_messages('incorrect_parameter_number')
            else:
                source_file_name = parts[1]
                bucket_name = parts[2]
                if argc == 4:
                    dest_object_name = parts[3]
                response = self.upload(source_file_name, bucket_name, dest_object_name)
        elif parts[0] == 'download':
            # Figure out parameters from command_string
            # dest_object_name and bucket_name are compulsory; source_file_name is optional
            # Use self._error_messages['incorrect_parameter_number'] if number of parameters is less
            # than number of compulsory parameters
            source_file_name = ''
            bucket_name = ''
            dest_object_name = ''

            argc = len(parts)
            if argc < 3 or argc > 4:
                response = self._error_messages('incorrect_parameter_number')
            else:
                dest_object_name = parts[1]
                bucket_name = parts[2]
                if argc == 4:
                    source_file_name = parts[3]
                response = self.download(dest_object_name, bucket_name, source_file_name)
        elif parts[0] == 'delete':
            dest_object_name = ''
            bucket_name = ''

            argc = len(parts)
            if not argc == 3:
                response = self._error_messages('incorrect_parameter_number')
            else:
                dest_object_name = parts[1]
                bucket_name = parts[2]
                response = self.delete(dest_object_name, bucket_name)
        elif parts[0] == 'deletedir':
            bucket_name = ''

            argc = len(parts)
            if argc < 2:
                response = self._error_messages('bucket_name_empty')
            elif argc > 2:
                response = self._error_messages('incorrect_parameter_number')
            else:
                bucket_name = parts[1]
                response = self.deletedir(bucket_name)
        elif parts[0] == 'find':
            file_extension = ''
            bucket_name = ''

            argc = len(parts)
            if argc < 2 or argc > 3:
                response = self._error_messages('incorrect_parameter_number')
            else:
                file_extension = parts[1]
                if argc == 3:
                    bucket_name = parts[2]
                response = self.find(file_extension, bucket_name)
        elif parts[0] == 'listdir':
            bucket_name = ''

            argc = len(parts)
            if argc > 2:
                response = self._error_messages('incorrect_parameter_number')
            else:
                if argc == 2:
                    bucket_name = parts[1]
                response = self.listdir(bucket_name)
        else:
            response = "Command not recognized."
        return response


def main():

    s3_handler = S3Handler()
    
    while True:
        try:
            command_string = ''
            if sys.version_info[0] < 3:
                command_string = raw_input("Enter command ('help' to see all commands, 'exit' to quit)>")
            else:
                command_string = input("Enter command ('help' to see all commands, 'exit' to quit)>")
    
            # Remove multiple whitespaces, if they exist
            command_string = " ".join(command_string.split())
            
            if command_string == 'exit':
                print("Good bye!")
                exit()
            elif command_string == 'help':
                s3_handler.help()
            else:
                response = s3_handler.dispatch(command_string)
                print(response)
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main()