from s3_handler import S3Handler

def execute(s3_handler, command, toggle=True):
	if not toggle:
		return ''

	response = ''
	
	try:
		response = s3_handler.dispatch(command)

	except Exception as e:
		response = e

	print(response)

def main():
	s3_handler = S3Handler()

	bucket_name = 'charles-gong-assignment2-bucket1'
	bucket2_name = 'charles-gong-assignment2-bucket2'
	bucket3_name = 'charles-gong-assignment2-bucket3'


	# Test - Create Directory

	command = 'createdir ' + bucket_name
	execute(s3_handler, command, False)

	command = 'createdir ' + bucket2_name
	execute(s3_handler, command, False)

	command = 'createdir ' + bucket3_name
	execute(s3_handler, command, False)

	# Test - Upload

	command = 'upload test/test.txt ' + bucket_name + ' test.txt'
	execute(s3_handler, command, False)

	command = 'upload test/test.txt ' + bucket_name + ' d1/test1.txt'
	execute(s3_handler, command, False)

	command = 'upload test/helloworld.md ' + bucket_name + ' hello.md'
	execute(s3_handler, command, False)

	command = 'upload test/helloworld.md ' + bucket2_name + ' hello'
	execute(s3_handler, command, False)

	command = 'upload test/test.txt ' + bucket2_name + ' random.txt'
	execute(s3_handler, command, False)


	# Test - Download

	command = 'download test.txt ' + bucket_name + ' test/download.txt'
	execute(s3_handler, command, False)

	command = 'download test.txt ' + bucket_name
	execute(s3_handler, command, False)

	command = 'download asjdfkas ' + bucket_name
	execute(s3_handler, command, False)

	command = 'download test.txt faskfj'
	execute(s3_handler, command, False)

	command = 'download test.txt'
	execute(s3_handler, command, False)

	command = 'download test.txt ' + bucket_name + ' kjdlkajsf slkfal'
	execute(s3_handler, command, False)


	# Test - Delete

	command = 'delete test.txt ' + bucket_name
	execute(s3_handler, command, False)

	command = 'delete d1/test.txt ' + bucket_name
	execute(s3_handler, command, False)

	command = 'delete sdads ' + bucket_name
	execute(s3_handler, command, False)

	command = 'delete test.txt fsafd'
	execute(s3_handler, command, False)

	command = 'delete test.txt'
	execute(s3_handler, command, False)

	command = 'delete test.txt ' + bucket_name + ' asfsdf'
	execute(s3_handler, command, False)

	command = 'delete hello ' + bucket_name
	execute(s3_handler, command, False)


	# Test - Delete Directory

	command = 'deletedir ' + bucket_name
	execute(s3_handler, command, False)

	command = 'deletedir fdafdfd'
	execute(s3_handler, command, False)


	# Test - Find

	command = 'find txt'
	execute(s3_handler, command, False)

	command = 'find txt ' + bucket_name
	execute(s3_handler, command, False)

	command = 'find zip ' + bucket_name
	execute(s3_handler, command, False)

	command = 'find'
	execute(s3_handler, command, False)

	command = 'find txt ' + bucket_name + ' asdfsd'
	execute(s3_handler, command, False)

	command = 'find md ' + bucket3_name
	execute(s3_handler, command, True)


	# Test - List Directory

	command = 'listdir ' + bucket3_name
	execute(s3_handler, command, True)

	command = 'listdir ' + bucket2_name
	execute(s3_handler, command, False)

	command = 'listdir'
	execute(s3_handler, command, False)

	command = 'listdir dlksfajls asdjfldsj'
	execute(s3_handler, command, False)

	command = 'listdir sdfa'
	execute(s3_handler, command, False)


if __name__ == '__main__':
	main()
