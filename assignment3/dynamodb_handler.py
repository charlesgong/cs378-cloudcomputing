from __future__ import print_function
import boto3
import json
import sys
import decimal
from datetime import datetime

# Note - Do not change the class name and constructor
# You are free to add any functions to this class without changing the specifications mentioned below.
class DynamoDBHandler:

	def __init__(self, region):
		self.client = boto3.client('dynamodb')
		self.resource = boto3.resource('dynamodb', region_name=region)

	def create_and_load_data(self, tableName, fileName):
		# TODO - This function should create a table named <tableName> 
		# and load data from the file named <fileName>
		
		# Create table
		response = self.client.create_table(
			TableName=tableName,
			KeySchema=[
				{
					'AttributeName': 'year',
					'KeyType': 'HASH'  # Partition key
				},
				{
					'AttributeName': 'title',
					'KeyType': 'RANGE'  # Sort key
				}
			],
			AttributeDefinitions=[
				{
					'AttributeName': 'year',
					'AttributeType': 'N'
				},
				{
					'AttributeName': 'title',
					'AttributeType': 'S'
				},

			],
			ProvisionedThroughput={
				'ReadCapacityUnits': 10,
				'WriteCapacityUnits': 10
			}
		)

		# Load data
		table = self.resource.Table(tableName)
		print("Creating table: " + tableName)

		try:
			table.wait_until_exists()
		except Exception as e:
			return e

		with open(fileName) as json_file:
			movies = json.load(json_file, parse_float=decimal.Decimal)
			for movie in movies:
				year = int(movie['year'])
				title = movie['title']
				info = movie['info']

				print("Adding movie:", year, title)

				table.put_item(
				   Item={
					   'year': year,
					   'title': title,
					   'info': info,
					}
				)

	def dispatch(self, command_string):
		# TODO - This function takes in as input a string command (e.g. 'insert_movie')
		# the return value of the function should depend on the command
		# For commands 'insert_movie', 'delete_movie', 'update_movie', delete_table' :
		#       return the message as a string that is expected as the output of the command
		# For commands 'search_movie_actor', 'search_movie_actor_director', print_stats' :
		#       return the a list of json objects where each json object has only the required
		#       keys and attributes of the expected result items.

		# Note: You should not print anything to the command line in this function.
		response = None

		if command_string == 'insert_movie':
			year = get_input("Year>")
			title = get_input("Title>")
			directors = get_input("Directors>")
			actors = get_input("Actors>")
			release_date = get_input("Release Date>")
			rating = get_input("Rating>")
			# Check parameters
			response = self._insert_movie(year, title, directors, actors, release_date, rating)
		
		elif command_string == 'delete_movie':
			title = get_input("Title>")
			response = self._delete_movie(title)

		elif command_string == 'update_movie':
			year = get_input("Year>")
			title = get_input("Title>")
			directors = get_input("Directors>")
			actors = get_input("Actors>")
			release_date = get_input("Release Date>")
			rating = get_input("Rating>")
			# Check parameters
			response = self._update_movie(year, title, directors, actors, release_date, rating)

		elif command_string == 'search_movie_actor':
			actor = get_input("Actor>")
			response = self._search_movie_actor(actor)
			if not response:
				response = self._error_messages('no_actor', actor)
		
		elif command_string == 'search_movie_actor_director':
			actor = get_input("Actor>")
			director = get_input("Director>")
			response = self._search_movie_actor_director(actor, director)
			if not response:
				response = self._error_messages('no_actor_director', actor, director)

		elif command_string == 'print_stats':
			pass

		elif command_string == 'delete_table':
			table_name = get_input("Table Name>")
			response = self._delete_table(table_name)

		else:
			response = self._error_messages('unknown_command')

		return response

	def help(self):
		print("Supported Commands:")
		print("1. insert_movie")
		print("2. delete_movie")
		print("3. update_movie")
		print("4. search_movie_actor")
		print("5. search_movie_actor_director")
		print("6. print_stats")
		print("7. delete_table")

	def _insert_movie(self, year, title, directors, actors, release_date, rating):
		directors_list = directors.split(',')
		actors_list = actors.split(',')
		release_datetime = self._std_datetime(release_date)

		table = self.resource.Table('Movies')
		table.put_item(
			Item={
				'year': int(year),
				'title': title,
				'info': {
					'directors': directors_list,
					'actors': actors_list,
					'release_date': release_datetime,
					'rating': decimal.Decimal(rating)
				}
			}
		)

		return self._success_messages('insert_movie', title)

	def _delete_movie(self, title):
		found = False

		fe = boto3.dynamodb.conditions.Key('title').eq(title)
		pe = 'title, #yr'
		ean = { '#yr': 'year' }

		table = self.resource.Table('Movies')
		response = table.scan(
			FilterExpression=fe,
			ProjectionExpression=pe,
			ExpressionAttributeNames=ean
		)
		
		for movie in response['Items']:
			found = True
			year = movie['year']
			# TODO = title to_lower_case
			title = movie['title']

			delete_response = table.delete_item(
				Key={
					'year': year,
					'title': title
				}
			)

		while 'LastEvaluatedKey' in response:
			response = table.scan(
				FilterExpression=fe,
				ProjectionExpression=pe,
				ExpressionAttributeNames=ean,
				ExclusiveStartKey=response['LastEvaluatedKey']
			)

			for movie in response['Items']:
				found = True
				year = movie['year']
				# TODO = title to_lower_case
				title = movie['title']

				delete_response = table.delete_item(
					Key={
						'year': year,
						'title': title
					}
				)

		if found:
			return self._success_messages('deleted_movie', title)
		else:
			return self._error_messages('title_not_exists', title)

	def _update_movie(self, year, title, directors, actors, release_date, rating):
		directors_list = directors.split(',')
		actors_list = actors.split(',')
		release_datetime = self._std_datetime(release_date)

		ue = 'SET info.directors = :dir, info.actors = :actors, info.#d = :date, info.rating = :rating'
		eav = {
			':dir': directors_list,
			':actors': actors_list,
			':date': release_datetime,
			':rating': decimal.Decimal(rating)
		}
		ean = { '#d': 'date' }

		table = self.resource.Table('Movies')
		try:
			response = table.update_item(
				Key={
					'year': int(year),
					'title': title
				},
				UpdateExpression=ue,
				ExpressionAttributeValues=eav,
				ExpressionAttributeNames=ean
			)
		except Exception as e:
			# self._error_messages('movie_not_updated', title, 'reason')
			return self._error_messages('movie_not_exists', title, year)
		
		return self._success_messages('updated_movie', title)

	def _search_movie_actor(self, actor):
		movies = []
		
		fe = boto3.dynamodb.conditions.Attr('info.actors').contains(actor)
		pe = 'title, #yr, info.actors'
		ean = { "#yr": "year" }

		table = self.resource.Table('Movies')
		response = table.scan(
			FilterExpression=fe,
			ProjectionExpression=pe,
			ExpressionAttributeNames=ean
		)

		movies += response['Items']

		# for i in response['Items']:
		# 	print(json.dumps(i, cls=DecimalEncoder))

		while 'LastEvaluatedKey' in response:
			response = table.scan(
				FilterExpression=fe,
				ProjectionExpression=pe,
				ExpressionAttributeNames=ean,
				ExclusiveStartKey=response['LastEvaluatedKey']
			)

			movies += response['Items']

			# for i in response['Items']:
			# 	print(json.dumps(i, cls=DecimalEncoder))

		return movies

	def _search_movie_actor_director(self, actor, director):		
		movies = []

		fe = boto3.dynamodb.conditions.Attr('info.actors').contains(actor) & boto3.dynamodb.conditions.Attr('info.directors').contains(director)
		pe = 'title, #yr, info.actors, info.directors'
		ean = { "#yr": "year" }

		table = self.resource.Table('Movies')
		response = table.scan(
			FilterExpression=fe,
			ProjectionExpression=pe,
			ExpressionAttributeNames=ean
		)
		
		movies = response['Items']
		
		# for i in response['Items']:
		# 	print(json.dumps(i, cls=DecimalEncoder))

		while 'LastEvaluatedKey' in response:
			response = table.scan(
				FilterExpression=fe,
				ProjectionExpression=pe,
				ExpressionAttributeNames=ean,
				ExclusiveStartKey=response['LastEvaluatedKey']
			)

			movies = response['Items']

			# for i in response['Items']:
			# 	print(json.dumps(i, cls=DecimalEncoder))

		return movies

	def _print_stats(self):
		pass

	def _delete_table(self, table_name):
		table = self.resource.Table(table_name)
		try:
			table.delete()
		except Exception as e:
			return self._error_messages('table_not_exists', table_name)

		return self._success_messages('deleted_table', table_name)

	def _std_datetime(self, date):
		return datetime.strptime(date, '%d %b %Y').strftime('%Y-%m-%d') + 'T00:00:00Z'

	def _error_messages(self, issue, *args):
		error = None

		if issue == 'unknown_command':
			error = "Unknown command entered. Type 'help' to see supported commands."
		elif issue == 'movie_not_inserted':
			error = "Movie {} could not be inserted - {}.".format(args[0], args[1])
		elif issue == 'title_not_exists':
			error = "Movie with {} does not exist.".format(args[0])
		elif issue == 'movie_not_deleted':
			error = "Movie {} could not be deleted - {}.".format(args[0], args[1])
		elif issue == 'movie_not_updated':
			error = "Movie {} could not be updated - {}.".format(args[0], args[1])
		elif issue == 'movie_not_exists':
			error = "Movie with {} and {} does not exist.".format(args[0], args[1])
		elif issue == 'no_actor':
			error = 'No movies found for actor {}.'.format(args[0])
		elif issue == 'no_actor_director':
			error = 'No movies found for actor {} and director {}.'.format(args[0], args[1])
		elif issue == 'table_not_exists':
			error = 'Table {} does not exist.'.format(args[0])
		else:
			error = 'Something was not correct with the request. Try again.'

		return error

	def _success_messages(self, success, *args):
		msg = None

		if success == 'insert_movie':
			msg = "Movie {} successfully inserted.".format(args[0])
		elif success == 'deleted_movie':
			msg = "Movie {} successfully deleted.".format(args[0])
		elif success == 'updated_movie':
			msg = "Movie {} successfully updated.".format(args[0])
		elif success == 'deleted_table':
			msg = "Table {} successfully deleted.".format(args[0])
		return msg

class DecimalEncoder(json.JSONEncoder):
	def default(self, o):
		if isinstance(o, decimal.Decimal):
			if o % 1 > 0:
				return float(o)
			else:
				return int(o)
		return super(DecimalEncoder, self).default(o)

def get_input(description):
	input_string = ''
	if sys.version_info[0] < 3:
		input_string = raw_input(description)
	else:
		input_string = input(description)

	return input_string

def output(out):
	if isinstance(out, str):
		print(out)
	elif isinstance(out, list):
		for i in out:
			m = json.dumps(i, cls=DecimalEncoder)
			print(m)
	else:
		print('Unexpected error.')

def main():
	# TODO - implement the main function so that the required functionality for the program is achieved.

	if len(sys.argv) < 2:
		print("No region specified. Please provide a region name.\nExiting.") # Error message
		exit()

	region = sys.argv[1]
	dynamodb_handler = DynamoDBHandler(region)
	try:
		dynamodb_handler.create_and_load_data('Movies', 'moviedata.json') # Error message
	except Exception as e:
		if e.response['Error']['Code'] != 'ResourceInUseException':
			raise e

	while True:
		try:
			command_string = ''
			if sys.version_info[0] < 3:
				command_string = raw_input("Enter command ('help' to see all commands, 'exit' to quit)>")
			else:
				command_string = input("Enter command ('help' to see all commands, 'exit' to quit)>")
			
			if command_string == 'exit':
				print("Good bye!")
				exit()
			elif command_string == 'help':
				dynamodb_handler.help()
			else:
				response = dynamodb_handler.dispatch(command_string)
				output(response)
		except Exception as e:
			print(e)


if __name__ == '__main__':
	main()
