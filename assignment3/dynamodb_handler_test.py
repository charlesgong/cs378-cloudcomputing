from dynamodb_handler import DynamoDBHandler

def execute(handler, command, inputs=[], toggle=True):
	if not toggle:
		return

	response = ''
	try:
		response = handler.dispatch(command)

	except Exception as e:
		response = e

	print(response)

def main():
	region = 'us-east-1'

	dynamodb_handler = DynamoDBHandler(region)
	try:
		dynamodb_handler.create_and_load_data('Movies', 'moviedata.json')
	except Exception as e:
		if e.response['Error']['Code'] != 'ResourceInUseException':
			exit()

	# Test - Create Directory

	command = 'insert_movie'
	execute(dynamodb_handler, command,
		'2018',
		'Black Panther',
		'Ryan Coogler',
		['Chadwick Bosman', 'Michael B. Jordan', 'Lupita Nyong\'o']),
		'16 February 2018',
		'7.8'
	)

if __name__ == '__main__':
	main()
