import os

import mysql.connector

class MoviesDB:
	def __init__(self):
		self._db = os.environ.get('RDS_DB_NAME', None)
		self._username = os.environ.get("RDS_USERNAME", None)
		self._password = os.environ.get("RDS_PASSWORD", None)
		self._hostname = os.environ.get("RDS_HOSTNAME", None)

	def __connect(self):
		return mysql.connector.connect(
			user=self._username,
			password=self._password,
			host=self._hostname,
			database=self._db
		)

	def insert_movie(self, title, year, director, actor, release_date, rating):
		stmt = '''
			INSERT INTO movies (title, year, director, actor, release_date, rating) 
			VALUES ('{}', {}, '{}', '{}', '{}', {});
		'''.format(
			title, year, director, actor, release_date, rating
		)

		cnx = None
		try:
			cnx = self.__connect()
			cur = cnx.cursor()
			cur.execute(stmt)
			cnx.commit()
		except mysql.connector.Error as e:
			raise e
		finally:
			if cur:
				cur.close()
			if cnx:
				cnx.close()

	def update_movie(self, title, year, director, actor, release_date, rating):			
		stmt = '''
			UPDATE movies
			SET year = {}, director = '{}', actor = '{}', release_date = '{}', rating = {}
			WHERE title = '{}';
		'''.format(
			year, director, actor, release_date, rating, title
		)

		cnx = None
		try:
			cnx = self.__connect()
			cur = cnx.cursor()
			cur.execute(stmt)
			cnx.commit()

			if cur.rowcount < 1:
				raise MoviesDBError('Movie does not exist')
		except mysql.connector.Error as e:
			raise e
		finally:
			if cur:
				cur.close()
			if cnx:
				cnx.close()

	def delete_movie(self, title):
		stmt = '''
			DELETE FROM movies WHERE title = '{}';
		'''.format(
			title
		)

		cnx = None
		try:
			cnx = self.__connect()
			cur = cnx.cursor()
			cur.execute(stmt)
			cnx.commit()

			if cur.rowcount < 1:
				raise MoviesDBError('Movie does not exist')
		except mysql.connector.Error as e:
			raise e
		finally:
			if cur:
				cur.close()
			if cnx:
				cnx.close()

	def query_actor(self, actor):
		res = []

		stmt = '''
			SELECT title, year, actor FROM movies WHERE actor = '{}';
		'''.format(
			actor
		)

		cnx = None
		try:
			cnx = self.__connect()
			cur = cnx.cursor(buffered=True)
			cur.execute(stmt)
			cnx.commit()

			if cur.rowcount < 1:
				raise MoviesDBError('No movies found for actor')

			res = cur.fetchall()
		except mysql.connector.Error as e:
			raise e
		finally:
			if cur:
				cur.close()
			if cnx:
				cnx.close()

		return res

	def highest_rating(self):
		res = []

		stmt = '''
			SELECT * FROM movies INNER JOIN (
				SELECT MAX(rating) AS max_rating
				FROM movies
			) sub
			ON movies.rating = sub.max_rating;
		'''

		cnx = None
		try:
			cnx = self.__connect()
			cur = cnx.cursor(buffered=True)
			cur.execute(stmt)
			cnx.commit()

			res = cur.fetchall()
		except mysql.connector.Error as e:
			raise e
		finally:
			if cur:
				cur.close()
			if cnx:
				cnx.close()

		return res

	def lowest_rating(self):
		res = []

		stmt = '''
			SELECT * FROM movies INNER JOIN (
				SELECT MIN(rating) AS min_rating
				FROM movies
			) sub
			ON movies.rating = sub.min_rating;
		'''

		cnx = None
		try:
			cnx = self.__connect()
			cur = cnx.cursor(buffered=True)
			cur.execute(stmt)
			cnx.commit()

			res = cur.fetchall()
		except mysql.connector.Error as e:
			raise e
		finally:
			if cur:
				cur.close()
			if cnx:
				cnx.close()

		return res


class MoviesDBError(Exception):
	def __init__(self, msg):
		super(MoviesDBError, self).__init__(msg)
