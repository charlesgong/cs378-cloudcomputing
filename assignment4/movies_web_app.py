from datetime import datetime

from flask import Flask, render_template, request
import mysql.connector
from movies_db import MoviesDB, MoviesDBError

application = Flask(__name__)
db = MoviesDB()

# API Endpoints

@application.route('/')
def index(msg='Choose an action'):
	return render_template('index.html', message=msg)

@application.route('/add_movie', methods=['POST'])
def add_movie():
	msg = request.form

	title = msg['title']
	year = msg['year']
	director = msg['director']
	actor = msg['actor']
	release_date = msg['release_date']
	rating = msg['rating']

	res = ''
	try:
		db.insert_movie(title, year, director, actor, std_datetime(release_date), rating)
		res = success_msg('insert_movie', title)
	except mysql.connector.Error as e:
		res = error_msg('insert_movie', title, str(e))

	return index(res)

@application.route('/update_movie', methods=['POST'])
def update_movie():
	msg = request.form

	title = msg['title']
	year = msg['year']
	director = msg['director']
	actor = msg['actor']
	release_date = msg['release_date']
	rating = msg['rating']

	res = ''
	try:
		db.update_movie(title, year, director, actor, std_datetime(release_date), rating)
		res = success_msg('update_movie', title)
	except mysql.connector.Error as e:
		res = error_msg('update_movie', title, str(e))
	except MoviesDBError as e:
		res = error_msg('movie_not_found', title)

	return index(res)

@application.route('/delete_movie', methods=['POST'])
def delete_movie():
	msg = request.form

	title = msg['delete_title']

	res = ''
	try:
		db.delete_movie(title)
		res = success_msg('delete_movie', title)
	except mysql.connector.Error as e:
		res = error_msg('delete_movie', title, str(e))
	except MoviesDBError as e:
		res = error_msg('movie_not_found', title)

	return index(res)

@application.route('/search_movie', methods=['GET'])
def search_movie():
	args = request.args

	actor = args['search_actor']

	res = []
	try:
		movies = db.query_actor(actor)
		res = [dict(title=row[0], year=str(row[1]), actor=row[2]) for row in movies]
	except mysql.connector.Error as e:
		err = error_msg('search_movie', title, str(e))
		return index(err)
	except MoviesDBError as e:
		err = error_msg('actor_not_found', actor)
		return index(err)

	return render_template('index.html', search_movies=res)

@application.route('/highest_rating', methods=['GET'])
def highest_rating():
	res = []
	try:
		movies = db.highest_rating()
		res = [
			dict(
				title=row[1],
				year=str(row[2]),
				director=row[3],
				actor=row[4],
				rating=str(row[6])
			)
			for row in movies
		]
	except mysql.connector.Error as e:
		err = error_msg('highest_rating', str(e))
		return index(err)

	return render_template('index.html', print_movies=res)

@application.route('/lowest_rating', methods=['GET'])
def lowest_rating():
	res = []
	try:
		movies = db.lowest_rating()
		res = [
			dict(
				title=row[1],
				year=str(row[2]),
				director=row[3],
				actor=row[4],
				rating=str(row[6])
			)
			for row in movies
		]
	except mysql.connector.Error as e:
		err = error_msg('lowest_rating', str(e))
		return index(err)

	return render_template('index.html', print_movies=res)


# Helpers

def std_datetime(date):
	return datetime.strptime(date, '%d %b %Y').strftime('%Y-%m-%d')

def success_msg(action, *args):
	msg = ''
	if action == 'insert_movie':
		msg = 'Movie {} successfully inserted'.format(args[0])
	elif action == 'update_movie':
		msg = 'Movie {} successfully updated'.format(args[0])
	elif action == 'delete_movie':
		msg = 'Movie {} successfully deleted'.format(args[0])

	return msg

def error_msg(action, *args):
	msg = ''
	if action == 'insert_movie':
		msg = 'Movie {} could not be inserted - {}'.format(args[0], args[1])
	elif action == 'update_movie':
		msg = 'Movie {} could not be updated - {}'.format(args[0], args[1])
	elif action == 'delete_movie':
		msg = 'Movie {} could not be deleted - {}'.format(args[0], args[1])
	elif action == 'search_movie':
		msg = 'Could not search movies for actor {} - {}'.format(args[0], args[1])
	elif action == 'highest_rating':
		msg = 'Could not find highest movie rating - {}'.format(args[0])
	elif action == 'lowest_rating':
		msg = 'Could not find lowest movie rating - {}'.format(args[0])
	elif action == 'movie_not_found':
		msg = 'Movie {} does not exist'.format(args[0])
	elif action == 'actor_not_found':
		msg = 'No movies found for actor {}'.format(args[0])

	return msg

if __name__ == '__main__':
	application.debug = True
	application.run(host='0.0.0.0')
